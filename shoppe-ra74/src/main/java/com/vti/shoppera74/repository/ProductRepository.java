package com.vti.shoppera74.repository;

import com.vti.shoppera74.modal.entity.Product;
import com.vti.shoppera74.modal.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository <Product, Long>, JpaSpecificationExecutor<Product> {
    List<Product> findByProductNameContainsAndProductTypeIn(String productName, List<ProductType> productTypes);

    boolean existsByProductName(String productName);
}
