package com.vti.shoppera74.modal.entity;

public enum ShippingUnit {
    EXPRESS, FAST, SAVE
}
