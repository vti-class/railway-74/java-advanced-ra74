package com.vti.shoppera74.modal.entity;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {// Biến thuộc tính này dùng để làm phân quyền trong Security
    USER, MANAGER, ADMIN;

    @Override
    public String getAuthority() {
        return name();
    }

//    A -> B || A có tất cả những gì thằng B có. thằng A có thể đại diện cho B
}
