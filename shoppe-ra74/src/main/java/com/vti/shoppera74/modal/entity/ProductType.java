package com.vti.shoppera74.modal.entity;

public enum ProductType {
    PHONE, COMPUTER, CLOTHES, FOOT_WEAR
}
