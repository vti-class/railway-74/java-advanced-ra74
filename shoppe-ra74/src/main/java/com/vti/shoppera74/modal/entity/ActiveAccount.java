//package com.vti.shoppera74.modal.entity;
//
//import lombok.Data;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//
//@Entity
//@Data
//public class ActiveAccount {
//    @Id
//    @Column(name = "id", nullable = false)
//    private Long id;
//    private String tokenToActive; // : Chuỗi bất kỳ ngẫu nhiên... duy nhát, not null
//    private Long accointId; // Khoá ngoại tới bảng Account
//
//    // API active: http:domain/active/{giá trị tokenToActive}{accountID} -> lấy ra được tokenToActive -> findByTokenToActive
//    // -> đối tươngh ActiveAccount -> lấy ra được Account cần Active -> update status -> ACTIVE
//}
