package com.vti.shoppera74.modal.entity;


import lombok.Data;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.util.Date;

@Data // Có getter, setter, toString
@MappedSuperclass // Đánh dấu class này cũng là 1 phần của entity kế thừa tới nó
public class EntityBase {
    @Column(name = "CREATE_BY")
    protected String createBy;

    @Column(name = "CREATE_DATE")
    protected Date createDate;

    @Column(name = "UPDATE_BY")
    protected String updateBy;

    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.DATE)
    protected Date updateDate;

    @PrePersist
    public void prePersist(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        this.createDate = new Date();
        this.createBy = username;
    }

    @PreUpdate
    public void preUpdate(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        this.updateDate = new Date();
        this.updateBy = username;
    }
}
