package com.vti.shoppera74.modal.entity;

public enum ProductStatus {
    OLD, NEW
}
