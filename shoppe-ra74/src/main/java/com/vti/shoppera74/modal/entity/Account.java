package com.vti.shoppera74.modal.entity;


import com.vti.shoppera74.modal.dto.AccountCreateDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "ACCOUNT2")
@Getter // Sinh ra các method getter
@Setter// Sinh ra các method setter
@NoArgsConstructor // Sinh ra hàm khởi tạo không tham số
@AllArgsConstructor // Sinh ra hàm khởi tạo có tất cả các tham số
public class Account extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "USERNAME", columnDefinition = "nvarchar(100)", nullable = false, unique = true)
    private String username;

    @Column(name = "FULL_NAME", columnDefinition = "nvarchar(100)")
    private String fullName;

    @Column(name = "AVATAR", length = 500)
    private String avatar;

    @Column(name = "ADDRESS", columnDefinition = "nvarchar(200)")
    private String address;

    @Column(name = "DATE_OF_BIRTH")
    private LocalDate dateBirth;

    @Column(name = "INFORMATION", columnDefinition = "nvarchar(500)")
    private String information;

    @Column(name = "PASSWORD", length = 100)
    private String password;

    @Column(name = "PHONE_NUMBER", length = 12)
    private String phoneNumber;

    @Column(name = "ROLE")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "MAIL",unique = true,nullable = false, length = 100)
    private String mail;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private AccountStatus accountStatus;

    public Account(AccountCreateDto createDto){
        this.username = createDto.getUsername();
        //....
    }
}


