package com.vti.shoppera74.modal.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class AccountUpdateDto {
    private long id;
    private String fullName;
    private String avatar;
    private String address;
    private LocalDate dateBirth;
    private String information;
    private String password;
    private String phoneNumber;
}
