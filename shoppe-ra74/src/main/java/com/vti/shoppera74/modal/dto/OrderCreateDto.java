package com.vti.shoppera74.modal.dto;

import lombok.Data;

import javax.validation.constraints.Positive;

@Data
public class OrderCreateDto {
    private long productId;
    private long accountId;

    @Positive(message = "Số lượng sp > 0")
    private int quantity;
}
