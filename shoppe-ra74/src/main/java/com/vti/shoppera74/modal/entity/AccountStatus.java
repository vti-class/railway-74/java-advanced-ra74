package com.vti.shoppera74.modal.entity;

public enum AccountStatus {
    ACTIVE, DE_ACTIVE, PENDING
}
