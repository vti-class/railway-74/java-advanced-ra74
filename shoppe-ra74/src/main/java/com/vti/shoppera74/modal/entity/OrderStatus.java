package com.vti.shoppera74.modal.entity;

public enum OrderStatus {
    PENDING, DONE, CANCEL
}
