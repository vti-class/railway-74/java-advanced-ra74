package com.vti.shoppera74.modal.dto;

import com.vti.shoppera74.modal.entity.ProductStatus;
import com.vti.shoppera74.modal.entity.ProductType;
import com.vti.shoppera74.modal.entity.ShippingUnit;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ProductCreatDto {
    @NotBlank(message = "{product.image.not_blank}")
    private String image;

    @NotBlank(message = "Tên sản phẩm không được để trống !")
    private String productName;
    private long price;
    private ProductType productType;
    private ShippingUnit shippingUnit;
    private ProductStatus productStatus;
}
