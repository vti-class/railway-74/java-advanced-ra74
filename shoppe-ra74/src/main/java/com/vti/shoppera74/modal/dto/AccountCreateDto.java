package com.vti.shoppera74.modal.dto;

import com.vti.shoppera74.validate.annotation.CheckUsername;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
public class AccountCreateDto {
    @NotBlank(message = "Tên người dùng không được để trống")
    @CheckUsername(message = "Người dùng đã tồn tại") //Check Username có tồn tại trong DB chưa, nếu rồi -> lỗi
    private String username;

    private String fullName;
    private String avatar;
    private String address;
    private LocalDate dateBirth;
    private String information;
    private String password;
    private String phoneNumber;

    private String mail;
}
