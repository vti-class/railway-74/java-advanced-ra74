package com.vti.shoppera74.modal.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT")
@Getter // Sinh ra các method getter
@Setter// Sinh ra các method setter
@NoArgsConstructor // Sinh ra hàm khởi tạo không tham số
@AllArgsConstructor // Sinh ra hàm khởi tạo có tất cả các tham số
public class Product extends EntityBase{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "IMAGE", length = 500)
    private String image;

    @Column(name = "PRODUCT_NAME", columnDefinition = "nvarchar(100)", nullable = false, unique = true)
    private String productName;

    @Column(name = "PRICE")
    private long price;

    @Column(name = "PRODUCT_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private ProductType productType;

    @Column(name = "SHIPPING_UNIT", nullable = false)
    @Enumerated(EnumType.STRING)
    private ShippingUnit shippingUnit;

    @Column(name = "PRODUCT_STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private ProductStatus productStatus;
}
