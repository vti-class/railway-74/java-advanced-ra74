package com.vti.shoppera74.modal.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "`ORDER`")
@Getter // Sinh ra các method getter
@Setter// Sinh ra các method setter
@NoArgsConstructor // Sinh ra hàm khởi tạo không tham số
@AllArgsConstructor // Sinh ra hàm khởi tạo có tất cả các tham số
//@Builder
public class Order extends EntityBase{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`")
    private long id;

    @Column(name = "ORDER_STATUS")
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Column(name = "`QUANTITY`")
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;
}
