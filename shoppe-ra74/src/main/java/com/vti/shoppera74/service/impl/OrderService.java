package com.vti.shoppera74.service.impl;

import com.vti.shoppera74.config.exception.CustomException;
import com.vti.shoppera74.config.exception.ErrorResponseEnum;
import com.vti.shoppera74.modal.dto.OrderCreateDto;
import com.vti.shoppera74.modal.entity.Account;
import com.vti.shoppera74.modal.entity.Order;
import com.vti.shoppera74.modal.entity.OrderStatus;
import com.vti.shoppera74.modal.entity.Product;
import com.vti.shoppera74.repository.AccountRepository;
import com.vti.shoppera74.repository.OrderRepository;
import com.vti.shoppera74.repository.ProductRepository;
import com.vti.shoppera74.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService implements IOrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Order create(OrderCreateDto createDto) {
        // Lấy ra đối tượng Product trong DB từ productId
        Optional<Product> productOptional = productRepository.findById(createDto.getProductId());
        if (productOptional.isEmpty()){
            throw new CustomException(ErrorResponseEnum.NOT_FOUND_PRODUCT);
        }
        // Lấy ra đối tượng Account....
        Optional<Account> accountOptional = accountRepository.findById(createDto.getAccountId());
        if (accountOptional.isEmpty()){
            throw new CustomException(ErrorResponseEnum.NOT_FOUND_ACCOUNT);
        }

        Product product = productOptional.get();
        Account account = accountOptional.get();

        Order order = new Order();
//        order.builder().product(product)
//                .account(account)
//                .quantity(createDto.getQuantity());
        order.setProduct(product);
        order.setAccount(account);
        order.setQuantity(createDto.getQuantity());
        order.setOrderStatus(OrderStatus.PENDING);
        return orderRepository.save(order);
    }

    @Override
    public List<Order> getListOrder(long accountId, OrderStatus orderStatus) {
        if (orderStatus == null){
            return orderRepository.findAllByAccount_IdOrderByIdDesc(accountId);
        }
        return orderRepository.findAllByOrderStatusAndAccount_Id(orderStatus, accountId);
    }

    @Override
    public Order buy(Long orderId) {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isEmpty()){
            throw new CustomException(ErrorResponseEnum.NOT_FOUND_ORDER);
        }
        Order order = optionalOrder.get();
        order.setOrderStatus(OrderStatus.DONE);
        return orderRepository.save(order);
    }

    @Override
    public Order cancel(Long orderId) {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isEmpty()){
            throw new CustomException(ErrorResponseEnum.NOT_FOUND_ORDER);
        }
        Order order = optionalOrder.get();
        order.setOrderStatus(OrderStatus.CANCEL);
        return orderRepository.save(order);
    }
}
