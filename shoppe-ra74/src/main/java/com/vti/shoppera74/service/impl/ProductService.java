package com.vti.shoppera74.service.impl;

import com.vti.shoppera74.config.exception.CustomException;
import com.vti.shoppera74.config.exception.ErrorResponseEnum;
import com.vti.shoppera74.modal.dto.ProductCreatDto;
import com.vti.shoppera74.modal.dto.ProductUpdateDto;
import com.vti.shoppera74.modal.dto.SearchProductRequest;
import com.vti.shoppera74.modal.entity.Product;
import com.vti.shoppera74.repository.ProductRepository;
import com.vti.shoppera74.repository.specification.ProductSpecification;
import com.vti.shoppera74.service.IProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService implements IProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Page<Product> getAll(int page, int size, String sortBy, String sortType) {
        Sort sort = null;
        if ("ASC".equalsIgnoreCase(sortType)){
            sort = Sort.by(sortBy).ascending();
        } else {
            sort = Sort.by(sortBy).descending();
        }
        // Mới chỉ có phân trang, chưa có sắp xếp
//        Pageable pageable = Pageable.ofSize(size).withPage(page-1);

        // Tạo ra đối tượng Pageable có sắp xếp
        Pageable pageable = PageRequest.of(page-1, size, sort);
        return productRepository.findAll(pageable);
    }

    @Override
    public Page<Product> search(SearchProductRequest request) {
        // Tạo ra điều kiện từ class ProductSpecification
        Specification<Product> condition = ProductSpecification.buildCondition(request);

        // Tạo phân trang, sắp xếp
        Sort sort = null;
        if ("ASC".equalsIgnoreCase(request.getSortType())){
            sort = Sort.by(request.getSortBy()).ascending();
        } else {
            sort = Sort.by(request.getSortBy()).descending();
        }
        // Tạo ra đối tượng Pageable có sắp xếp
        Pageable pageable = PageRequest.of(request.getPage()-1, request.getSize(), sort);
        return productRepository.findAll(condition, pageable);
    }

    @Override
    public Product getById(long id) {
        if (id < 0){
            throw new CustomException(400, "ID ko được là số âm");
        }
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isEmpty()){
            throw new CustomException(ErrorResponseEnum.NOT_FOUND_PRODUCT);
        }
        return productOptional.get();
    }

    @Override
    public Product create(ProductCreatDto createDto) {
//        -------- Validate/ verify / kiểm tra - dữ liệu đầu vào --------
//        (Validate những điều kiện mà java có thể check được trước,
//        rồi mới validate những điều kiện ở database - Mục đích -> Tối tưu thời gian)
//        if (createDto.getImage().length() > 500){
//            throw new CustomException(400, "Độ dài url ảnh ko được quá 500 ký tự");
//        }
        // kiểm tra sư tồn tại của productNAme...
        boolean checkProductName = productRepository.existsByProductName(createDto.getProductName());
        if (checkProductName){
            throw new CustomException(ErrorResponseEnum.PRODUCT_NAME_EXISTED);
        }
//        ------------------------------------------------
        Product product = new Product();
        BeanUtils.copyProperties(createDto, product);
        return productRepository.save(product);
    }

    @Override
    public Product update(ProductUpdateDto updateDto) {
        if(!productRepository.existsById(updateDto.getId())){
            throw new CustomException(ErrorResponseEnum.NOT_FOUND_PRODUCT);
        }
        // kiểm tra sự tồn tại của productID
        Product product = new Product();
        BeanUtils.copyProperties(updateDto, product);
        return productRepository.save(product);
    }

    @Override
    public void delete(long id) {
        productRepository.deleteById(id);
    }
}