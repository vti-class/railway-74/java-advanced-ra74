package com.vti.shoppera74.service;

import com.vti.shoppera74.modal.dto.OrderCreateDto;
import com.vti.shoppera74.modal.entity.Order;
import com.vti.shoppera74.modal.entity.OrderStatus;

import java.util.List;

public interface IOrderService {
    Order create(OrderCreateDto createDto);

    List<Order> getListOrder(long accountId, OrderStatus orderStatus);

    Order buy(Long orderId);

    Order cancel(Long orderId);
}
