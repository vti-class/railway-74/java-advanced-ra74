package com.vti.shoppera74.service;

import com.vti.shoppera74.modal.dto.*;
import com.vti.shoppera74.modal.entity.Account;
import com.vti.shoppera74.modal.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IProductService {
    Page<Product> getAll(int page, int size, String sortBy, String sortType);

    Page<Product> search(SearchProductRequest request);

    Product getById(long id);

    Product create(ProductCreatDto createDto);

    Product update(ProductUpdateDto updateDto);

    void delete(long id);
}
