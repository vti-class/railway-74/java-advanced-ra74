package com.vti.shoppera74.service.impl;

import com.vti.shoppera74.modal.dto.AccountCreateDto;
import com.vti.shoppera74.modal.dto.AccountUpdateDto;
import com.vti.shoppera74.modal.entity.Account;
import com.vti.shoppera74.modal.entity.Role;
import com.vti.shoppera74.repository.AccountRepository;
import com.vti.shoppera74.service.IAccountService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackOn = Exception.class)
public class AccountService implements IAccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account getById(long id) {
        return accountRepository.findById(id).get();
    }

    @Override
    public Account create(AccountCreateDto createDto) {
        Account account = new Account();
        BeanUtils.copyProperties(createDto, account);
        account.setRole(Role.USER);
        return accountRepository.save(account);
    }

    @Override
    public Account update(AccountUpdateDto updateDto) {
        return null;
    }

    @Override
    public void delete(long id) {
        accountRepository.deleteById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Account> accountOptional = accountRepository.findByUsername(username);
        if (accountOptional.isEmpty()){ // Nếu username không tồn tại -> bắn ra lỗi
            throw new UsernameNotFoundException(username);
        }
        Account account = accountOptional.get();

        // Tạo ra đối tượng UserDetails ( là đối tượng mà hàm loadUserByUsername muốn trả về) từ account
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(account.getRole());
        return new User(account.getUsername(), account.getPassword(), authorities);
    }
}
