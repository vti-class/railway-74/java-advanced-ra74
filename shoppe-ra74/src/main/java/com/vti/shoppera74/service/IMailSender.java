package com.vti.shoppera74.service;

public interface IMailSender {
    void sendMessageWithAttachment(String to, String subject, String text);
}
