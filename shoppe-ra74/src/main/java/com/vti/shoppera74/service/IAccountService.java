package com.vti.shoppera74.service;

import com.vti.shoppera74.modal.dto.AccountCreateDto;
import com.vti.shoppera74.modal.dto.AccountUpdateDto;
import com.vti.shoppera74.modal.entity.Account;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IAccountService extends UserDetailsService {
    List<Account> getAll();

    Account getById(long id);

    Account create(AccountCreateDto createDto);

    Account update(AccountUpdateDto updateDto);

    void delete(long id);
}
