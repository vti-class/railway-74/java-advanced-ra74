package com.vti.shoppera74.controller;

import com.vti.shoppera74.config.exception.CustomException;
import com.vti.shoppera74.modal.dto.AccountCreateDto;
import com.vti.shoppera74.modal.dto.ProductCreatDto;
import com.vti.shoppera74.modal.dto.ProductUpdateDto;
import com.vti.shoppera74.modal.dto.SearchProductRequest;
import com.vti.shoppera74.modal.entity.Account;
import com.vti.shoppera74.modal.entity.Product;
import com.vti.shoppera74.service.IAccountService;
import com.vti.shoppera74.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
@CrossOrigin("*")
@Validated
public class ProductController {
    @Autowired
    private IProductService productService;

    @GetMapping("/get-all")
    public Page<Product> getAll(int page, int size, String sortBy, String sortType){
        return productService.getAll(page, size, sortBy, sortType);
    }

    @PostMapping("/search")
    public Page<Product> search(@RequestBody SearchProductRequest request){
        return productService.search(request);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
            return ResponseEntity.ok(productService.getById(id));// Thành công, http status: 200
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PostMapping("/create")
    public Product create(@RequestBody @Valid ProductCreatDto createDto){
        return productService.create(createDto);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PutMapping("/update")
    public Product update(@RequestBody @Valid ProductUpdateDto updateDto){
        return productService.update(updateDto);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        productService.delete(id);
    }
}
