package com.vti.shoppera74.controller;

import com.vti.shoppera74.modal.dto.AccountCreateDto;
import com.vti.shoppera74.modal.entity.Account;
import com.vti.shoppera74.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/account")
@Validated
public class AccountController {
    @Autowired
    private IAccountService accountService;

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @GetMapping("/get-all")
    public List<Account> getAll(){
        return accountService.getAll();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @GetMapping("/{id}")
    public Account getById(@PathVariable long id){
        return accountService.getById(id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PostMapping("/create")
    public Account create(@RequestBody @Valid AccountCreateDto createDto){
        return accountService.create(createDto);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        accountService.delete(id);
    }
}
