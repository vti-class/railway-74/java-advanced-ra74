package com.vti.shoppera74.controller;

import com.vti.shoppera74.config.exception.CustomException;
import com.vti.shoppera74.config.exception.ErrorResponseEnum;
import com.vti.shoppera74.modal.dto.AccountCreateDto;
import com.vti.shoppera74.modal.dto.AccountLoginResponse;
import com.vti.shoppera74.modal.entity.Account;
import com.vti.shoppera74.modal.entity.AccountStatus;
import com.vti.shoppera74.modal.entity.Role;
import com.vti.shoppera74.repository.AccountRepository;
import com.vti.shoppera74.service.impl.MailSenderService;
import com.vti.shoppera74.utils.JWTTokenUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin("*")
@Validated
public class AuthController {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private JWTTokenUtils jwtTokenUtils;

    @Autowired
    private MailSenderService mailSenderService;

    @PostMapping("/register")
    public Account register(@RequestBody @Valid AccountCreateDto createDto){
        if (accountRepository.existsByUsername(createDto.getUsername())){
            throw new CustomException(ErrorResponseEnum.USERNAME_EXISTED);
        }
        Account account = new Account();
        BeanUtils.copyProperties(createDto, account);
        account.setRole(Role.USER);
        account.setAccountStatus(AccountStatus.PENDING);
        // Mã hoá mật khẩu rồi lưu vào DB
        String passwordEncoder = new BCryptPasswordEncoder().encode(createDto.getPassword());
        account.setPassword(passwordEncoder);
        account = accountRepository.save(account);
        // THêm logic: Gửi mail để kích hoạt tài khoản
        // http:domain/active/bacnadscbahkjdvnakldjhvbhjadsbv
        String subject = "KÍCH HOẠT TÀI KHOẢN!";
        String api = "http://localhost:8888/api/v1/auth/active/" + account.getId();
        String content = "<img src=\"https://vtiacademy.edu.vn/upload/images/academy-02-01-01-01.png\">\n" +
                "<div>Bạn đã đăng ký tài khoản trên shoppe VTI. Để kích hoạt tài khoản, <a href=\""+ api +"\" target=\"_blank\">Click vào đây</a><div>";
        mailSenderService.sendMessageWithAttachment(account.getMail(), subject, content);
        return account;
    }

    @GetMapping("/login")
    public String login(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        return "Bạn đã login thành công: " + username;
    }

    @GetMapping("/active/{accountId}") // API để active tài khoản http://localhost:8888/api/v1/auth/active/accountID
    public String activeAccount(@PathVariable Long accountId){
        Account account = accountRepository.findById(accountId).get();
        account.setAccountStatus(AccountStatus.ACTIVE);
        accountRepository.save(account);
        return "Tài khoản đã được kích hoạt!";
    }

    @PostMapping("/login-v2")
    public AccountLoginResponse loginV2(@RequestParam String username,@RequestParam String password){
        Optional<Account> accountOptional = accountRepository.findByUsername(username);
        if (accountOptional.isEmpty()){
            throw new CustomException(ErrorResponseEnum.LOGIN_USERNAME_NOT_EXISTED);
        }
        Account account = accountOptional.get();

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        boolean checkPassword = passwordEncoder.matches(password, account.getPassword());
        if (!checkPassword){
            throw new CustomException(ErrorResponseEnum.LOGIN_PASSWORD_FALSE);
        }
        AccountLoginResponse response = new AccountLoginResponse();
        BeanUtils.copyProperties(account, response);
        return response;
    }

    @PostMapping("/login-jwt")
    public AccountLoginResponse loginJWT(@RequestParam String username,@RequestParam String password){
        // Bước1: Kiểm tra username
        Optional<Account> accountOptional = accountRepository.findByUsername(username);
        if (accountOptional.isEmpty()){
            throw new CustomException(ErrorResponseEnum.LOGIN_USERNAME_NOT_EXISTED);
        }
        Account account = accountOptional.get();

        // Bước 2: Kiểm tra password
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        boolean checkPassword = passwordEncoder.matches(password, account.getPassword());
        if (!checkPassword){
            throw new CustomException(ErrorResponseEnum.LOGIN_PASSWORD_FALSE);
        }
        // B3: Kiểm tra account có được active hay ko
        if (account.getAccountStatus() != AccountStatus.ACTIVE){
            throw new CustomException(ErrorResponseEnum.ACCOUNT_NOT_ACTIVE);
        }

        AccountLoginResponse response = new AccountLoginResponse();
        BeanUtils.copyProperties(account, response);

        // Bước3: Tạo ra token
        String token = jwtTokenUtils.createAccessToken(response);
        // Bước 4: set token vào AccountLoginResponse -> return
        response.setToken(token);
        return response;
    }



}
