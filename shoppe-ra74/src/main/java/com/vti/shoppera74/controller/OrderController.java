package com.vti.shoppera74.controller;

import com.vti.shoppera74.modal.dto.OrderCreateDto;
import com.vti.shoppera74.modal.entity.Order;
import com.vti.shoppera74.modal.entity.OrderStatus;
import com.vti.shoppera74.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/order")
@Validated
@CrossOrigin("*")
public class OrderController {
    @Autowired
    private IOrderService orderService;

    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("/create")
    public Order create(@RequestBody @Valid OrderCreateDto createDto){
        return orderService.create(createDto);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/get-by-status")
    public List<Order> getListOrder(@RequestParam(required = false) OrderStatus orderStatus, long accountId){
        return orderService.getListOrder(accountId, orderStatus);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("/buy-product/{orderId}")
    public Order buy(@PathVariable Long orderId){
        return orderService.buy(orderId);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("/cancel-product/{orderId}")
    public Order cancel(@PathVariable Long orderId){
        return orderService.cancel(orderId);
    }
}
