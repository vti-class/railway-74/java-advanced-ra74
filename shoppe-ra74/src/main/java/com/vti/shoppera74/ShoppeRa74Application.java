package com.vti.shoppera74;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ShoppeRa74Application {

    public static void main(String[] args) {
        SpringApplication.run(ShoppeRa74Application.class, args);
    }

}
