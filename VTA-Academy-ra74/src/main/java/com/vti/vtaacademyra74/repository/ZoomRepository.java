package com.vti.vtaacademyra74.repository;

import com.vti.vtaacademyra74.modal.entity.Zoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZoomRepository extends JpaRepository<Zoom, Long> {
    List<Zoom> findAllByNameContains(String name);
}
