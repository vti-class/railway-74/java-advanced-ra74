package com.vti.vtaacademyra74;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VtaAcademyRa74Application {

    public static void main(String[] args) {
        SpringApplication.run(VtaAcademyRa74Application.class, args);
    }

}
