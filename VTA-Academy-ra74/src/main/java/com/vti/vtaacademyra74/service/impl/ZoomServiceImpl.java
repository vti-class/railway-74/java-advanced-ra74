package com.vti.vtaacademyra74.service.impl;

import com.vti.vtaacademyra74.modal.dto.CreateZoomDto;
import com.vti.vtaacademyra74.modal.entity.Zoom;
import com.vti.vtaacademyra74.repository.ZoomRepository;
import com.vti.vtaacademyra74.service.IZoomService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service //(Tương đương với @Repository || @Component || ...)// Tạo ra 1 bean (Giá trị của class ZoomServiceImpl) -> Spring IoC
public class ZoomServiceImpl implements IZoomService {
    @Autowired // Lấy 1 bean (giá trị) trong Sprign IoC -> gán vào trong thuộc tính bên dưới
    private ZoomRepository zoomRepository;

    @Override
    public List<Zoom> getAll() {
        return zoomRepository.findAll();
    }

    @Override
    public List<Zoom> search(String name) {
        if (name != null) {
            return zoomRepository.findAllByNameContains(name);
        } else {
            return zoomRepository.findAll();
        }
    }

    @Override
    public Zoom create(CreateZoomDto dto) {
        Zoom zoom = new Zoom();

        // Set các giá trị của th Dto cho đối tượng Entity
//        zoom.setName(dto.getName());
        BeanUtils.copyProperties(dto, zoom);
        // Lưu vaoif DB
        return zoomRepository.save(zoom);
    }
}
