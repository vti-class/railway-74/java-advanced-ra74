package com.vti.vtaacademyra74.service;

import com.vti.vtaacademyra74.modal.dto.CreateZoomDto;
import com.vti.vtaacademyra74.modal.entity.Zoom;

import java.util.List;

public interface IZoomService {
    List<Zoom> getAll();

    List<Zoom> search(String name);

    Zoom create(CreateZoomDto dto);
}
