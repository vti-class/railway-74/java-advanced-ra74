package com.vti.vtaacademyra74.modal.dto;

import lombok.Data;

@Data
public class CreateZoomDto {

    private String link;
    private String description;
    private String note;
    private String meetingId;
    private String passCode;
    private String name;
}
