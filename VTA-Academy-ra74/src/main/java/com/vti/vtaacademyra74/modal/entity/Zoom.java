package com.vti.vtaacademyra74.modal.entity;

import lombok.Data;

import javax.persistence.*;

@Entity // Để đánh dấu class zoom là 1 table trong DB (bắt buộc)
@Table(name = "ZOOM") // Để đánh dấu class Zoom ứng với table nào trong DB (ko bắt buộc)
@Data// Sinh ra các method: Getter, setter, toString,..
public class Zoom extends EntityBase{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME", length = 50, nullable = false, unique = true) // Đánh dấu thuộc tính này tương ứng với cột nào trong DB
    private String name;

    @Column(name = "LINK", length = 200, nullable = false, unique = true)
    private String link;

    @Column(name = "DESCRIPTION", length = 500)
    private String description;

    @Column(name = "NOTE", length = 500)
    private String note;

    @Column(name = "MEETING_ID", length = 15, nullable = false, unique = true)
    private String meetingId;

    @Column(name = "PASS_CODE", length = 15, nullable = false, unique = true)
    private String passCode;
}
