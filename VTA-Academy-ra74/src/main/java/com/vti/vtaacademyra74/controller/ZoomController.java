package com.vti.vtaacademyra74.controller;

import com.vti.vtaacademyra74.modal.dto.CreateZoomDto;
import com.vti.vtaacademyra74.modal.entity.Zoom;
import com.vti.vtaacademyra74.service.IZoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController // Cũng tạo 1 bean, dành riêng cho việc tạo ra 1 API (Rest Full API >< MVC)
@RequestMapping("/api/v1/zoom") // KHai báo đường dẫn chung cho toàn đối tượng ZoomController
@CrossOrigin("*")
public class ZoomController {
    @Autowired
    private IZoomService zoomService;

    @GetMapping("/get-all") // http:localhost//api/v1/zoom/get-all GET
    public List<Zoom> getAll(){
        return zoomService.getAll();
    }

    @GetMapping("/search") // http:localhost//api/v1/zoom/get-all GET
    public List<Zoom> search(@RequestParam(required = false) String name){
        return zoomService.search(name);
    }

    @PostMapping("/create")
    public Zoom create(@RequestBody CreateZoomDto dto){
        return zoomService.create(dto);
    }
}
