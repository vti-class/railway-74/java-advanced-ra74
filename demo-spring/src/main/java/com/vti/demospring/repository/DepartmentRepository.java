package com.vti.demospring.repository;

import com.vti.demospring.modal.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository //ko có vẫn hoạt động được BT
public interface DepartmentRepository extends JpaRepository<Department, Integer> {

}
