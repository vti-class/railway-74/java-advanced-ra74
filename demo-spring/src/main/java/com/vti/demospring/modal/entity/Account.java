package com.vti.demospring.modal.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ACCOUNT")
@Getter // Sinh ra các method getter
@Setter// Sinh ra các method setter
@NoArgsConstructor // Sinh ra hàm khởi tạo không tham số
@AllArgsConstructor // Sinh ra hàm khởi tạo có tất cả các tham số
public class Account extends EntityBase{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "USERNAME", columnDefinition = "nvarchar(100)", nullable = false, unique = true)
    private String username;

    @Column(name = "AVATAR", length = 500)
    private String avatar;

    @Column(name = "ADDRESS", columnDefinition = "nvarchar(200)")
    private String address;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;
}
