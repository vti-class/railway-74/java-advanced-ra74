package com.vti.demospring.modal.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

@Data // Có getter, setter, toString
@MappedSuperclass // Đánh dấu class này cũng là 1 phần của entity kế thừa tới nó
public class EntityBase {
    @Column(name = "CREATE_BY")
    protected String createBy;

    @Column(name = "CREATE_DATE")
    protected Date createDate;

    @Column(name = "UPDATE_BY")
    protected String updateBy;

    @Column(name = "UPDATE_DATE")
    protected Date updateDate;

    @PrePersist
    public void prePersist(){
        this.createDate = new Date();
        this.createBy = "Mr.Uoc";
    }

    @PreUpdate
    public void preUpdate(){
        this.updateDate = new Date();
        this.updateBy = "Mr.Uoc";
    }
}
