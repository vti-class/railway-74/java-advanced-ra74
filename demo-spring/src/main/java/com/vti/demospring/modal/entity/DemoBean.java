package com.vti.demospring.modal.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
public class DemoBean {
    private int id;
    private String name;
}