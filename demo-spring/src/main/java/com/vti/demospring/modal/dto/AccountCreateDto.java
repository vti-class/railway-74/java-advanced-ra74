package com.vti.demospring.modal.dto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class AccountCreateDto {
    private String username;
    private String avatar;
    private String address;
    private Integer departmentId;
}
