package com.vti.demospring.controller;

import com.vti.demospring.modal.dto.AccountCreateDto;
import com.vti.demospring.modal.entity.Account;
import com.vti.demospring.modal.entity.DemoBean;
import com.vti.demospring.service.impl.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
@RestController
@RequestMapping("/api/v1/account") // Khai báo đường dẫn chung
@CrossOrigin("*")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @Autowired
//    @Qualifier("demoBean2")
    private DemoBean demoBean;

    //@RequestMapping(name = "", method = RequestMethod.GET)// localhost:{port đang sử dụng}/api/v1/account
    @GetMapping("/get-all")// localhost:{port đang sử dụng}/api/v1/account/get-all
    public List<Account> getAll(){

//        demoBean.setName(UUID.randomUUID().toString());

        return accountService.getAll();
    }

    @GetMapping("/{id}")// PathVariable // localhost:{port đang sử dụng}/api/v1/account/ biến ID
    public Account findById(@PathVariable long id){
        return accountService.findById(id);
    }

    @PostMapping
    public Account create(@RequestBody AccountCreateDto accountDto){
        return accountService.create(accountDto);
    }

    @PutMapping
    public Account update(@RequestBody Account account){
        return accountService.update(account);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        accountService.delete(id);
    }
}
