package com.vti.demospring.controller;

import com.vti.demospring.modal.entity.Department;
import com.vti.demospring.service.impl.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
@RestController
@RequestMapping("/api/v1/department")
@CrossOrigin("*")
public class DepartmentController {

//    @RequestMapping("/home")
//    public String homePage(){
////        ModelAndView modelAndView = new ModelAndView();
//
//        return "index2";
//    }
    @Autowired
    private DepartmentService departmentService;

    // Kiểu dữ liệu trả về của chức năng đó là gì?
    // Tên method
    // Có các biến truyền vào nào để thực hiện chức nang đó?

    // Thực hiện thao tác gọi tới chức năng tương ứng ở bên Service

    // Phương thức của API là gì?
    // Đường dẫn để gọi tới chức năng này là như nào?
    // Các biến đó được truyền theo cách nào? Truyền vào trong body (Nếu API có phương thức là get, thì ko dùng body),
    // Truyền biến ngay trên URL: PathvAriable, Request Param
    @GetMapping("/get-all") // http://localhost:8080/api/v1/departmen/get-all
    public List<Department> getAll(){
        return departmentService.getAll();
    }

    @GetMapping("/{id}")
    public Department getById(@PathVariable int id){
        return departmentService.getById(id);
    }

   @PostMapping ("/create")
    public Department create(@RequestBody Department department){
        return departmentService.create(department);
    }

    @PutMapping ("/update")
    public Department update(@RequestBody Department department){
        return departmentService.update(department);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        departmentService.delete(id);
    }
}
