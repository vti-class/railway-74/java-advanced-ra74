package com.vti.demospring.config;

import com.vti.demospring.modal.entity.DemoBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class ConfigDemo {
    @Bean
    @Primary
    public DemoBean demoBean1(){
        DemoBean demoBean = new DemoBean();
        demoBean.setId(1);
        demoBean.setName("ABC");
        return demoBean;
    }

    @Bean
    public DemoBean demoBean2(){
        DemoBean demoBean = new DemoBean();
        demoBean.setId(2);
        demoBean.setName("XYZ");
        return demoBean;
    }
}
