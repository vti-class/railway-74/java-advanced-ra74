package com.vti.demospring.service;

import com.vti.demospring.modal.entity.Department;

import java.util.List;

public interface IDepartmentService {
    // Kiểu dữ liệu trả về của chức năng đó là gì?
    // Tên method
    // Có các biến truyền vào nào để thực hiện chức nang đó?
    List<Department> getAll();

    Department getById(int id);

    Department create(Department department);

    Department update(Department department);

    void delete(int id);
}
