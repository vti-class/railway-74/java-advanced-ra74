package com.vti.demospring.service.impl;

import com.vti.demospring.modal.dto.AccountCreateDto;
import com.vti.demospring.modal.entity.Account;
import com.vti.demospring.modal.entity.Department;
import com.vti.demospring.repository.AccountRepository;
import com.vti.demospring.repository.DepartmentRepository;
import com.vti.demospring.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service // Spring IoC khai báo đây là 1 bean
public class AccountService implements IAccountService {

    @Autowired // Gán giá trị vào thuộc tính này
    private AccountRepository accountRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account findById(long id) {
        // check tồn tại của ID
        return accountRepository.findById(id).get();
    }

    @Override
    public Account create(AccountCreateDto accountDto) {
        // Lấy được đối tượng Department từ AccountCreateDto.departmentId
        Department department = null;
        if (accountDto.getDepartmentId() != null){
            department = departmentRepository.findById(accountDto.getDepartmentId()).get();
        }

        // Tạo ra đối tượng Account Entity (Các giá trị được lấy từ DTO) để lưu nó
        Account account = new Account();
        account.setAddress(accountDto.getAddress());
        account.setAvatar(accountDto.getAvatar());
        account.setUsername(accountDto.getUsername());
        account.setDepartment(department);

        return accountRepository.save(account);
    }

    @Override
    public Account update(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public void delete(long id) {
        accountRepository.deleteById(id);
    }
}
